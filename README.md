# Welcome

This is **CaosDB Docker**, a part of the CaosDB project.

If you want to try out CaosDB, you are at the right place.  For further
instruction and a 1-step-demo, please continue reading in
[README_SETUP.md](README_SETUP.md).


# Further Reading

Please refer to the [official gitlab repository of the CaosDB
project](https://gitlab.indiscale.com/caosdb/src) for more information.

# License

Copyright (C) 2022 IndiScale GmbH
Copyright (C) 2022 Daniel Hornung, Timm Fitschen, Henrik tom Wörden.

All files in this repository are licensed under the [GNU Affero General Public
License](LICENCE.md) (version 3 or later).

