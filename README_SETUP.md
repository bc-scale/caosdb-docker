# Getting Started #

## Requirements ##
We use docker and docker-compose to deploy CaosDB:
- Docker >= 18.02
- docker-compose >= 1.21

On Debian based systems, you can install the requirements with:

```sh
sudo apt-get install docker.io docker-compose
```

Note, that your user account needs to be in the docker group to start a
container. Security note: The docker group is equivalent to root permissions.

```sh
sudo usermod -aG docker $USER
```
You should log out after this step and log in again. You can check with the
command `groups` whether you have the docker group.

It is recommended that you clone this repository such that you have the
example configuration files at hand.

## Start CaosDB ##
Once you have docker installed, you only need two configuration files: One
for the reverse proxy envoy `envoy.yml` and one that defines the docker containers that
you want to use and how they can interact `docker-compose.yml`. If you did not
clone this repository before, you can copy the
`compose` folder of this repository to your computer and then run

```sh
docker-compose -f compose/docker-compose.yml up
```

This will download the images that are used which may take a couple of minutes.
Once the logs read

```
caosdb-server_1  | Starting the Jetty [HTTPS/1.1] server on port 10443
caosdb-server_1  | Starting the Jetty [HTTP/1.1] server on port 10080
caosdb-server_1  | Starting org.caosdb.server.CaosDBServer application
```

the server is ready and you can access CaosDB with your web browser under `localhost:8081`.
Default credentials are
- username: `admin`
- password: `caosdb`

## Stopping CaosDB ##
You can stop the containers with CTRL-C. See the documentation of docker-compose
for other options (e.g. `-d` for detach).

Note, that the suggested configuration provided here uses persistent volumes.
This means that data and files will be stored such that they are persistent
across restarts.


## Building the image

If you don't want to use the prebuilt image, e.g., because you changed something
in the Dockerfile, you can build the docker image yourself using the `docker`
command in the `docker` folder:

```sh
cd docker
DOCKER_BUILDKIT=1 docker build -t caosdb .
```

The buildkit setting allows the usage of some special features in the `Dockerfile`.

The image will be named `caosdb`.

### Versions of CaosDB components
If you want to build an image with specific versions of the various CaosDB
components, you can do so by using `--build-arg` options.
For example `--build-arg MYSQLBACKEND=v4.0.0` will change the version of the
sql backend.

The following arguments are available: `MYSQLBACKEND SERVER WEBUI PYLIB ADVANCEDUSERTOOLS`.

## SSL Certificates
If you want to use SSL certificates (and in production use, you definitely should)
you need to uncomment the respective sections in `envoy.yml` and `docker-compose.yml`.
The given configuration assumes, that the certificate files are placed in a folder
`compose/certs` and named `caosdb.key.pem` and `caosdb.cert.pem`.

It is assumed that `caosdb.key.pem` does not have a passphrase. You can remove
one with `openssl rsa -in oldKey.pem -out newKey.key`. If you want to use a
certificate with a passphrase instead, please refer to envoy's documentation as
to how to configure this correctly.

Make sure that envoy can read the certificate. You can set the UID for envoy in
the environment section of the envoy service in `docker-compose.yml`.

## Configuration

### The CaosDB server
You can include alternative configuration settings for the CaosDB server.
Please see the [server documentation](https://docs.indiscale.com/caosdb-server/administration/configuration.html).

It is recommended to add settings in files in the `server.conf.d` folder. E.g.
`caosdb-server/conf/ext/server.conf.d/50-example.conf`

Uncomment the corresponding section in the `docker-compose.yml`

### Including large data folders
Often users do not want to copy all files into CaosDB but instead want to make
whole filesystems available to the CaosDB server. This can be achieved by mounting
the corresponding filesystem into the container. You can do this by uncommenting
the volume section that bind mounts some path (that you must set) to `.../extroot`.



### Bind IP
If you want your server to be accessible from other computers, you need to change
the bind ip. Adjust the compose file accordingly.


## Integration Tests

## Troubleshooting

### Envoy cannot read SSL certificate files
Make sure that the user that is used by envoy has read permissions.
