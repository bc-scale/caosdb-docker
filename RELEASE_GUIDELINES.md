# Release Guidelines for the CaosDB Server

This document specifies release guidelines in addition to the general release
guidelines of the CaosDB Project
([RELEASE_GUIDELINES.md](https://gitlab.com/caosdb/caosdb/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* CHANGELOG.md is up-to-date.

## Steps

1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process. Naming: `release-<VERSION>`

2. Check all general prerequisites.

3. Update the versions in:
  * `CHANGELOG.md`
  * `compose/docker-compose.yml` in the `caosdb-server.image` section

5. Merge the release branch into the main branch.

6. Tag the latest commit of the main branch with `v<VERSION>`.

7. Delete the release branch.

8. Merge the main branch back into the dev branch and set the image tag to a `-dev` version.

9. Build the docker image according to the [`README_SETUP.md`](README_SETUP.md),
   tag it as `caosdb/caosdb:<version>`, and push it to docker hub.

10. Update the versions for the next developement round:
  * `CHANGELOG.md`: Re-add the `[Unreleased]` section.

11. Add a gitlab release in the respective repository:
    https://gitlab.indiscale.com/caosdb/src/caosdb-docker/-/releases

    Add a description, which can be a copy&paste from the CHANGELOG, possibly prepended by:

    ```md
# Changelog

[See full changelog](https://gitlab.indiscale.com/caosdb/src/caosdb-docker/-/blob/v0.7.3/CHANGELOG.md)
    ```
