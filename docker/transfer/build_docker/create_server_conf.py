#!/usr/bin/env python3

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Daniel Hornung, Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

import argparse
import os
import configparser

# Defaults appropriate for a Docker image
default_settings = {
    "MYSQL_USER_NAME": "caosdb",
     # replace later for productive use
    "MYSQL_USER_PASSWORD": "random1234",
    "MYSQL_DATABASE_NAME": "caosdb",
    "MYSQL_HOST": "sqldb",

    "SERVER_PORT_HTTP": "10080",
    "SERVER_PORT_HTTPS": "10443",

    # Change later or create key now
    "CERTIFICATES_KEY_PASSWORD": "CaosDBSecret",
    "CERTIFICATES_KEY_STORE_PATH": "/opt/caosdb/cert/caosdb.jks",
    # NOTE: Using the same PW for the moment, until the Java side is fixed.
    # "CERTIFICATES_KEY_STORE_PASSWORD": "CaosDBKeyStore",
    "CERTIFICATES_KEY_STORE_PASSWORD": "CaosDBSecret",
    "CHECK_ENTITY_ACL_ROLES_MODE": "SHOULD",

    # Paths concerning file storage
    "INSERT_FILES_IN_DIR_ALLOWED_DIRS": "/opt/caosdb/mnt/extroot",
    "FILE_SYSTEM_ROOT": "/opt/caosdb/mnt/caosroot",
    "DROP_OFF_BOX": "/opt/caosdb/mnt/dropoffbox",
    "TMP_FILES": "/tmp/caosdb/tmpfiles",
}

def _create_parser():
    """Creates the argument parser."""
    epilog ="""
Instead of command line arguments, environment variables are allowed as
well.  In this case, use the UPPER_CASE version of the argument, with hyphens
(-) replaced by underscores (_).  """
    parser = argparse.ArgumentParser(description='Create a config file.',
                                     epilog=epilog)
    parser.add_argument('conf_dir', type=str, help='The config directory')
    parser.add_argument("-c", "--certificate", action="store_true",
                        help="Should certificates be created as well?")

    for setting, default in default_settings.items():
        # Order of precedence: local < environment < command line
        parser.add_argument("--" + setting.lower().replace("_","-"),
                            dest=setting, default=os.environ.get(
                                setting, default))

    return parser


def _create_config(args):
    conf = ""
    for setting, _ in default_settings.items():
        conf += "{key}={val}\n".format(key=setting, val=getattr(args, setting))
    return conf

def main():
    parser = _create_parser()
    args = parser.parse_args()
    conf = _create_config(args)
    print(conf)

if __name__ == "__main__":
    main()
