#!/bin/bash

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Daniel Hornung, Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# To fail fast, but beware https://mywiki.wooledge.org/BashFAQ/105
set -e

############# Remotes #################
REM_MYSQLBACKEND="https://gitlab.indiscale.com/caosdb/src/caosdb-mysqlbackend.git"
REM_SERVER="https://gitlab.indiscale.com/caosdb/src/caosdb-server.git"
REM_PYLIB="https://gitlab.indiscale.com/caosdb/src/caosdb-pylib.git"
############# End of user configurables #################

function fail() {
    echo "Some error occured, exiting."
    exit 1
}

function make_caosdb() {
    # python client
    pushd git/caosdb-pylib
    popd
    # CaosDB itself
    pushd git/caosdb-server
    pushd caosdb-webui
    echo "Making webui"
    make
    popd
    pushd misc/pam_authentication/
    make
    popd
    popd
}

function make_run() {
    # Startup helper tools
    pushd run_docker/utils
    tools="
        start_nslcd
        ypsetup
        tzsetup
        mail_setup
        permission_setup
    "
    for tool in $tools; do
        g++ -std=c++17 -o "$tool" src/"$tool".cpp \
            -lboost_system -lboost_filesystem -ldl -lstdc++fs
        chown root "$tool"
        chmod u+s "$tool"
    done
    popd

    # Modify pam files
    nis_lines="
        passwd
        group
        shadow
    "
    for nis_line in $nis_lines; do
        sed -i -e 's/\(^'"${nis_line}"':.*\)/\1 nis/' /etc/nsswitch.conf
    done
}

function config_caosdb() {
    cd git/caosdb-server/conf/core
    ../../../../build_docker/create_server_conf.py . > ../ext/server.conf
    mkdir -p /tmp/caosdb/tmpfiles
    mkdir -p /opt/caosdb/mnt/caosdb-server
    mkdir -p /opt/caosdb/mnt/other
}

function prepare_sql() {
    cp build_docker/sql.config git/caosdb-mysqlbackend/.config
}


# Adds an admin user, including PAM setup in the usersources.ini
function user() {
    PASSWORD="caosdb"
    PASSWORDTU="caosdb"
    useradd admin
    useradd testuser
    echo -e "${PASSWORD}\n${PASSWORD}" | passwd admin
    echo -e "${PASSWORDTU}\n${PASSWORDTU}" | passwd testuser
    echo "Adding CaosDB user 'admin' with password 'caosdb'."
    INI_TXT=$(cat <<EOF
realms = PAM
defaultRealm = PAM
[PAM]
class = org.caosdb.server.accessControl.Pam
default_status = ACTIVE
include.user = admin
# , testuser
user.admin.roles = administration
# user.testuser.roles = administration
EOF
           )
    echo "$INI_TXT" > git/caosdb-server/conf/ext/usersources.ini
}

function cert() {
    ./$(dirname $0)/cert.sh
}

function drop_privilege() {
    chmod -R a+rwX cert
    cd git
    chmod -R a+rwX caosdb-server caosdb-mysqlbackend
    chmod -R a+rwX /tmp/caosdb/tmpfiles
	#TODO should not be needed in the container
    #chmod -R a+rwX /opt/caosdb/m2
    # We need this for the "PAM inside Docker" authentication
    chown :shadow \
          caosdb-server/misc/pam_authentication/bin/pam_authentication
    chmod g+s caosdb-server/misc/pam_authentication/bin/pam_authentication
}

case $1 in
    "os") echo "Not supported anymore, call setup_os.sh instead."; exit 1 ;;
    "clone") clone_caosdb ;;
    "config") config_caosdb ;;
    "make") make_caosdb ;;
    "make_run") make_run ;;
    "cert") cert ;;
    "prep_sql") prepare_sql ;;
    "user") user ;;
    "privilege") drop_privilege ;;
    *) echo "Unknown action: $1"; exit 1
esac
