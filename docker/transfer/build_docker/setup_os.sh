#!/bin/bash

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Daniel Hornung, Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# To fail fast, but beware https://mywiki.wooledge.org/BashFAQ/105
set -e

function setup_os() {
    # - mariadb-client :: For SQL server configuration.
    PACKAGES="
    build-essential
    curl
    gettext-base
    ldap-utils
    libboost-filesystem-dev
    libboost-system-dev
    libpam0g-dev
    default-jre
    make
    mariadb-client
    nis
    postfix
    rsync
    unzip
    "
    # vim etc :: For debugging / development
    PACKAGES+="
    bash-completion
    byobu
    emacs-nox
    htop
    procps
    vim
    "
    export DEBIAN_FRONTEND="noninteractive"

    # Re-enable caching, since we have cache-mounted directories now:
    # https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/experimental.md
    rm /etc/apt/apt.conf.d/docker-clean

    apt-get --allow-releaseinfo-change update
    apt-get dist-upgrade -y
    apt-get install -y $PACKAGES

    # For debugging / development
    echo ". /etc/bash_completion" >> /etc/bash.bashrc
}

# Just call the function
setup_os
