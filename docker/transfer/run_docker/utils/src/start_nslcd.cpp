// ** header v3.0
// This file is a part of the CaosDB Project.
//
// Copyright (C) 2019 Daniel Hornung
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ** end header

/**
 * This program sets the time zone to the given value and deletes itself afterwards
 */

#include <iostream>
#include <string>
#include <unistd.h>
#include <boost/dll.hpp>

// Compile it with (the order of the cpp and the libs matters:
// g++ -o start_nslcd src/start_nslcd.cpp -lboost_system -lboost_filesystem -ldl

int main(int argc, char* argv[]) {

  // Change the real user ID
  setuid(0);
  std::string start_nslcd_str = std::string("/etc/init.d/nslcd start");
  std::cout << start_nslcd_str << std::endl;
  std::system(start_nslcd_str.c_str());

  // We trust the first run of this program since we call it ourselves here.
  // Simply delete it after running. ;-)
  std::string delete_me = "/bin/rm " + boost::dll::program_location().string();
  std::cout << delete_me << std::endl;
  std::system(delete_me.c_str());
  return 0;
}
